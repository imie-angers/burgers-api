/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 50725
 Source Host           : localhost:3306
 Source Schema         : speedburger

 Target Server Type    : MySQL
 Target Server Version : 50725
 File Encoding         : 65001

 Date: 05/04/2019 16:57:00
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for account_order
-- ----------------------------
DROP TABLE IF EXISTS `account_order`;
CREATE TABLE `account_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `amount` double NOT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `paid` tinyint(1) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_C3A64AE9A76ED395` (`user_id`),
  CONSTRAINT `FK_C3A64AE9A76ED395` FOREIGN KEY (`user_id`) REFERENCES `account_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of account_order
-- ----------------------------
BEGIN;
INSERT INTO `account_order` VALUES (1, 0, 1, 1, NULL);
INSERT INTO `account_order` VALUES (2, 8.99, 1, 1, NULL);
INSERT INTO `account_order` VALUES (3, 8.99, 1, 1, 28);
INSERT INTO `account_order` VALUES (4, 8.99, 0, 1, 28);
INSERT INTO `account_order` VALUES (5, 8.99, NULL, 1, 28);
INSERT INTO `account_order` VALUES (6, 8.99, NULL, 1, 28);
INSERT INTO `account_order` VALUES (7, 8.99, NULL, 1, 28);
INSERT INTO `account_order` VALUES (8, 8.99, NULL, 1, 28);
INSERT INTO `account_order` VALUES (9, 8.99, NULL, 1, 28);
INSERT INTO `account_order` VALUES (10, 6.99, NULL, 0, NULL);
INSERT INTO `account_order` VALUES (11, 6.99, NULL, 1, 6);
INSERT INTO `account_order` VALUES (12, 6.99, NULL, 0, NULL);
INSERT INTO `account_order` VALUES (13, 9.98, NULL, 0, NULL);
INSERT INTO `account_order` VALUES (14, 9.98, NULL, 0, NULL);
INSERT INTO `account_order` VALUES (15, 6.99, NULL, 0, NULL);
INSERT INTO `account_order` VALUES (16, 6.99, NULL, 0, NULL);
INSERT INTO `account_order` VALUES (17, 10.48, NULL, 0, NULL);
INSERT INTO `account_order` VALUES (18, 21.46, NULL, 0, NULL);
INSERT INTO `account_order` VALUES (19, 6.99, NULL, 0, NULL);
INSERT INTO `account_order` VALUES (20, 6.99, NULL, 0, NULL);
INSERT INTO `account_order` VALUES (21, 6.99, NULL, 0, NULL);
INSERT INTO `account_order` VALUES (22, 6.99, NULL, 0, NULL);
INSERT INTO `account_order` VALUES (23, 6.99, NULL, 0, NULL);
INSERT INTO `account_order` VALUES (24, 12.48, NULL, 0, NULL);
COMMIT;

-- ----------------------------
-- Table structure for account_user
-- ----------------------------
DROP TABLE IF EXISTS `account_user`;
CREATE TABLE `account_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username_canonical` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_canonical` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `confirmation_token` varchar(180) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `firstname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_10051E392FC23A8` (`username_canonical`),
  UNIQUE KEY `UNIQ_10051E3A0D96FBF` (`email_canonical`),
  UNIQUE KEY `UNIQ_10051E3C05FB297` (`confirmation_token`),
  UNIQUE KEY `UNIQ_10051E3F5B7AF75` (`address_id`),
  CONSTRAINT `FK_10051E3F5B7AF75` FOREIGN KEY (`address_id`) REFERENCES `account_user_address` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of account_user
-- ----------------------------
BEGIN;
INSERT INTO `account_user` VALUES (5, 'antoine@sharemat.com', 'antoine@sharemat.com', 'antoine@sharemat.com', 'antoine@sharemat.com', 1, NULL, '$2y$13$7L1u0RSj6jFyH2kjZhgz5uCcLZrEVpnAXbS6Eu64KFd0cv5QUfGzC', '2019-04-04 14:52:16', NULL, NULL, 'a:1:{i:0;s:9:\"ROLE_USER\";}', 'Antoine', 'BOURLART', NULL, NULL);
INSERT INTO `account_user` VALUES (6, 'frederic.dinand@gmail.com', 'frederic.dinand@gmail.com', 'frederic.dinand@gmail.com', 'frederic.dinand@gmail.com', 1, NULL, '$2y$13$5yU9IR.4lehJHp3codjiPulmVhOYpYFwK5b2o4v1mWoGyUykRVp4K', '2019-04-05 14:56:55', NULL, NULL, 'a:1:{i:0;s:9:\"ROLE_USER\";}', 'Frédéric', 'Dinand', NULL, NULL);
INSERT INTO `account_user` VALUES (7, 'test@gmail.com', 'test@gmail.com', 'test@gmail.com', 'test@gmail.com', 1, NULL, '$2y$13$qcrCleAIBwHdol8TmsPEf.KKUqpYsNC1KwHaJM5swiuKzRSEsc0rG', '2019-04-05 09:20:48', NULL, NULL, 'a:1:{i:0;s:9:\"ROLE_USER\";}', 'Grégoire', 'Drapeau', NULL, NULL);
INSERT INTO `account_user` VALUES (8, 'ruben.lecomte@gmail.com', 'ruben.lecomte@gmail.com', 'ruben.lecomte@gmail.com', 'ruben.lecomte@gmail.com', 1, NULL, '$2y$13$zwPSan82lHxI04.CeEcgFOqCStASITsvh0mwfediYXit3ehlc9RRm', '2019-04-05 09:20:56', NULL, NULL, 'a:1:{i:0;s:9:\"ROLE_USER\";}', 'Ruben', 'Lecomte', NULL, NULL);
INSERT INTO `account_user` VALUES (9, 'camille.camille@gmail.com', 'camille.camille@gmail.com', 'camille.camille@gmail.com', 'camille.camille@gmail.com', 1, NULL, '$2y$13$XRSSGj3g73HxgmbP.bXt/.kEfSNtJ9GM0/UrFO.yvusDO9dD5JGaK', '2019-04-04 14:52:24', NULL, NULL, 'a:1:{i:0;s:9:\"ROLE_USER\";}', 'Camille', 'Camille', NULL, NULL);
INSERT INTO `account_user` VALUES (10, 'samuel.gilbert49@hotmail.com', 'samuel.gilbert49@hotmail.com', 'samuel.gilbert49@hotmail.com', 'samuel.gilbert49@hotmail.com', 1, NULL, '$2y$13$2.TYiwjiIBVouOBy23zWMuvK3t660ilYg.uafkapiGoEio3.ETH1G', '2019-04-04 14:23:35', NULL, NULL, 'a:1:{i:0;s:9:\"ROLE_USER\";}', 'SamuelAntoine', 'GILBERT', NULL, NULL);
INSERT INTO `account_user` VALUES (11, 'ema@a.fr', 'ema@a.fr', 'ema@a.fr', 'ema@a.fr', 1, NULL, '$2y$13$rB7kBAEVeVUlEL3lr9QlwO6thENSwcVr3gm.8VgqYJ27zO1gI.GiC', NULL, NULL, NULL, 'a:1:{i:0;s:9:\"ROLE_USER\";}', 'firstname', 'lastname', NULL, NULL);
INSERT INTO `account_user` VALUES (12, 'em7@a.fr', 'em7@a.fr', 'em7@a.fr', 'em7@a.fr', 1, NULL, '$2y$13$7kMmAw0QvRDV6pYZc3TPbOqOd.JBukuZ4NQET6tCauZTRl0jAmQt.', NULL, NULL, NULL, 'a:1:{i:0;s:9:\"ROLE_USER\";}', 'Augustin', 'Pottier', NULL, NULL);
INSERT INTO `account_user` VALUES (13, 'guillaume.lefebvre@imie.fr', 'guillaume.lefebvre@imie.fr', 'guillaume.lefebvre@imie.fr', 'guillaume.lefebvre@imie.fr', 1, NULL, '$2y$13$A3UaFdUPVpj1Ll6Uy2kWoOmaIaHF6GN2MWXEfyHqlrS0BY7PS.Obi', '2019-04-05 14:51:58', NULL, NULL, 'a:1:{i:0;s:9:\"ROLE_USER\";}', 'Guillaume', 'LEFEBVRE', NULL, NULL);
INSERT INTO `account_user` VALUES (14, 'em47@a.fr', 'em47@a.fr', 'em47@a.fr', 'em47@a.fr', 1, NULL, '$2y$13$cVhxv3ecujbgqouUNjFDVOQ4D8jYrRajbktba2Lm7McXjVB7c80H6', NULL, NULL, NULL, 'a:1:{i:0;s:9:\"ROLE_USER\";}', 'Augustin', 'Pottier', NULL, NULL);
INSERT INTO `account_user` VALUES (15, 'alice.hubert@imie.fr', 'alice.hubert@imie.fr', 'alice.hubert@imie.fr', 'alice.hubert@imie.fr', 1, NULL, '$2y$13$.4N27ocRP5/eWeAC53iybuktlrUKobqacZ/k7aGKyQ1WZOrHaTjxu', '2019-04-04 14:17:03', NULL, NULL, 'a:1:{i:0;s:9:\"ROLE_USER\";}', 'Alice', 'HUBERT', NULL, NULL);
INSERT INTO `account_user` VALUES (16, 'elisa.klein@imie.fr', 'elisa.klein@imie.fr', 'elisa.klein@imie.fr', 'elisa.klein@imie.fr', 1, NULL, '$2y$13$p9mjrw.fEmosUKqkF9rmyuZKN8MF9Z2z/12a.iN30vpnk/lFva3zm', '2019-04-05 14:20:58', NULL, NULL, 'a:1:{i:0;s:9:\"ROLE_USER\";}', 'Elisa', 'Klein', NULL, NULL);
INSERT INTO `account_user` VALUES (17, 'melanie.morin@imie.fr', 'melanie.morin@imie.fr', 'melanie.morin@imie.fr', 'melanie.morin@imie.fr', 1, NULL, '$2y$13$SBiMSAqeElcjsLWhkQoQBucq5Ri5.RhVU2VYDVB8HNf4b9Ul161L.', '2019-04-04 14:06:57', NULL, NULL, 'a:1:{i:0;s:9:\"ROLE_USER\";}', 'Melmo', ' ', NULL, NULL);
INSERT INTO `account_user` VALUES (18, 'em147@a.fr', 'em147@a.fr', 'em147@a.fr', 'em147@a.fr', 1, NULL, '$2y$13$c06ZwKEj4YF5GxKocg7odu7CfsEv1ZNLURN7zCPXEArVbosFz52ci', NULL, NULL, NULL, 'a:1:{i:0;s:9:\"ROLE_USER\";}', 'Augustin', 'Pottier', NULL, NULL);
INSERT INTO `account_user` VALUES (19, 'em1447@a.fr', 'em1447@a.fr', 'em1447@a.fr', 'em1447@a.fr', 1, NULL, '$2y$13$xLEyRM2jqd782lfnKYz.RudVwbQAUBFxFB6BQPukQ5brU8VXZ9WhG', NULL, NULL, NULL, 'a:1:{i:0;s:9:\"ROLE_USER\";}', 'Augustin', 'Pottier', NULL, NULL);
INSERT INTO `account_user` VALUES (20, 'em15447@a.fr', 'em15447@a.fr', 'em15447@a.fr', 'em15447@a.fr', 1, NULL, '$2y$13$lNv1i8mAzMz7lyc4GlQIuOv88N/2hYC2W3GnQM9gAbxbbi2JYcAya', NULL, NULL, NULL, 'a:1:{i:0;s:9:\"ROLE_USER\";}', 'Augustin', 'Pottier', NULL, NULL);
INSERT INTO `account_user` VALUES (21, 'adresse@mail.fr', 'adresse@mail.fr', 'adresse@mail.fr', 'adresse@mail.fr', 1, NULL, '$2y$13$ZK6FrknL.dIysB5ORX0nx.ih7wVgT9P1TQQvNRuLVR4O9UqTpIEoO', '2019-04-05 14:17:48', NULL, NULL, 'a:1:{i:0;s:9:\"ROLE_USER\";}', 'Hubert', 'René', NULL, NULL);
INSERT INTO `account_user` VALUES (22, 'alice.hubert@hotmail.fr', 'alice.hubert@hotmail.fr', 'alice.hubert@hotmail.fr', 'alice.hubert@hotmail.fr', 1, NULL, '$2y$13$zz3hcT7gezqSsVpobIuXAeyUgbM8Hl5y.pOWQNzqnXT6/N1oEsEUm', NULL, NULL, NULL, 'a:1:{i:0;s:9:\"ROLE_USER\";}', 'Alice', 'HUBERT', NULL, NULL);
INSERT INTO `account_user` VALUES (23, 'antoine@sharemat.fr', 'antoine@sharemat.fr', 'antoine@sharemat.fr', 'antoine@sharemat.fr', 1, NULL, '$2y$13$dwcoGEYKFqPNFndskdJv0OPxaMJsdJu9eBNbuVP82lJoFKajJfNT6', NULL, NULL, NULL, 'a:1:{i:0;s:9:\"ROLE_USER\";}', 'dergreg', 'dergreg', NULL, NULL);
INSERT INTO `account_user` VALUES (24, 'antoined@sharemat.fr', 'antoined@sharemat.fr', 'antoined@sharemat.fr', 'antoined@sharemat.fr', 1, NULL, '$2y$13$QSMRbtch0JF9UOr4peKxmOGmRx/aubMxkrUAmKnLVJf/9j.XdTZq2', NULL, NULL, NULL, 'a:1:{i:0;s:9:\"ROLE_USER\";}', 'dergreg', 'dergreg', NULL, NULL);
INSERT INTO `account_user` VALUES (25, 'antoinedf@sharemat.fr', 'antoinedf@sharemat.fr', 'antoinedf@sharemat.fr', 'antoinedf@sharemat.fr', 1, NULL, '$2y$13$/s6X60iXezOaTRDIzUS.F.2eKJjJ3YuIOtGiIsRgXAlciLjfMJaNu', NULL, NULL, NULL, 'a:1:{i:0;s:9:\"ROLE_USER\";}', 'dergreg', 'dergreg', NULL, 1);
INSERT INTO `account_user` VALUES (26, 'antoineddf@sharemat.fr', 'antoineddf@sharemat.fr', 'antoineddf@sharemat.fr', 'antoineddf@sharemat.fr', 1, NULL, '$2y$13$GRLzN/ek2fBQTFrlFNZ2v.ZbO3xNh5SgrsMhbFspwDaqkZjWE2KzG', NULL, NULL, NULL, 'a:1:{i:0;s:9:\"ROLE_USER\";}', 'dergreg', 'dergreg', NULL, 2);
INSERT INTO `account_user` VALUES (27, 'antoinedddf@sharemat.fr', 'antoinedddf@sharemat.fr', 'antoinedddf@sharemat.fr', 'antoinedddf@sharemat.fr', 1, NULL, '$2y$13$fcrrDz/rxQAv65g3.mKNXemuYZxjZxjmU1L7GqvGhvYVJmc1T4EgW', NULL, NULL, NULL, 'a:1:{i:0;s:9:\"ROLE_USER\";}', 'dergreg', 'dergreg', NULL, 3);
INSERT INTO `account_user` VALUES (28, 'antoine@gmail.com', 'antoine@gmail.com', 'antoine@gmail.com', 'antoine@gmail.com', 1, NULL, '$2y$13$sSf.3NAKbheuSMzlGv6hVuXk55udFmTpHe5RuxkOvRnPW0PE7y0Na', '2019-04-05 13:41:24', NULL, NULL, 'a:1:{i:0;s:9:\"ROLE_USER\";}', 'dergreg', 'dergreg', NULL, 4);
INSERT INTO `account_user` VALUES (29, 'em154z47@a.fr', 'em154z47@a.fr', 'em154z47@a.fr', 'em154z47@a.fr', 1, NULL, '$2y$13$nR7fzCC/Lsx138UKvkvT2OqB/FYC18ftOhbHtoHp7vkhbEmCx//Li', NULL, NULL, NULL, 'a:1:{i:0;s:9:\"ROLE_USER\";}', 'Augustin', 'Pottier', NULL, NULL);
INSERT INTO `account_user` VALUES (30, 'dzqdq@mail.fr', 'dzqdq@mail.fr', 'dzqdq@mail.fr', 'dzqdq@mail.fr', 1, NULL, '$2y$13$qxeDOVlT/WYYq5IoYtUY2ujiUyi5hCQGrJpvD5r.ZiCsQOQP6OQ.q', NULL, NULL, NULL, 'a:1:{i:0;s:9:\"ROLE_USER\";}', 'Hubert', 'René', NULL, NULL);
INSERT INTO `account_user` VALUES (31, 'dzqdq@maeil.fr', 'dzqdq@maeil.fr', 'dzqdq@maeil.fr', 'dzqdq@maeil.fr', 1, NULL, '$2y$13$cC7joIWxaoZX/xdWIrWvUOPRIOVPUYvKg.XElSAuYhuQVta6Sr3SG', NULL, NULL, NULL, 'a:1:{i:0;s:9:\"ROLE_USER\";}', 'Hubert', 'René', NULL, NULL);
INSERT INTO `account_user` VALUES (32, 'dzqdzqdqdz@mail.fr', 'dzqdzqdqdz@mail.fr', 'dzqdzqdqdz@mail.fr', 'dzqdzqdqdz@mail.fr', 1, NULL, '$2y$13$ebIIEbkmv3Sukvfwydu6puGcpIW5LUKvazIWSU0FD5u.Yvq/F5wsu', '2019-04-05 14:09:26', NULL, NULL, 'a:1:{i:0;s:9:\"ROLE_USER\";}', 'dzqdqz', 'zqdzqd', NULL, NULL);
INSERT INTO `account_user` VALUES (33, 'newuser@gmail.com', 'newuser@gmail.com', 'newuser@gmail.com', 'newuser@gmail.com', 1, NULL, '$2y$13$3EzReKAbQ7TzAc3opfHKXe8lRYIeU.tVkOIcCxT9C1IjAt21ZFai2', NULL, NULL, NULL, 'a:1:{i:0;s:9:\"ROLE_USER\";}', 'Test', 'Test', NULL, 5);
INSERT INTO `account_user` VALUES (34, 'nek@gmail.fr', 'nek@gmail.fr', 'nek@gmail.fr', 'nek@gmail.fr', 1, NULL, '$2y$13$tO0yhJERu2BPIK3QEfrFG.YD0eGrE9G6BQWR8Y77ZIKN8zQu.ssPa', NULL, NULL, NULL, 'a:1:{i:0;s:9:\"ROLE_USER\";}', 'Test', 'Test', NULL, 6);
INSERT INTO `account_user` VALUES (35, 'gregoire@gmail.com', 'gregoire@gmail.com', 'gregoire@gmail.com', 'gregoire@gmail.com', 1, NULL, '$2y$13$yX/ii3VsqFW/NfuVdXWlEeIUOXGhtXk.G2GxSj3lgQlLaVEEB65Xu', NULL, NULL, NULL, 'a:1:{i:0;s:9:\"ROLE_USER\";}', 'Grégoire', 'Drapeau', NULL, 7);
INSERT INTO `account_user` VALUES (36, 'gregoire.drapeau79@gmail.com', 'gregoire.drapeau79@gmail.com', 'gregoire.drapeau79@gmail.com', 'gregoire.drapeau79@gmail.com', 1, NULL, '$2y$13$Qr6DcUBss0mjTdP/yQLeg.8DEAbdlYnE7uxOxwpjJoLFu44LFRtZu', '2019-04-05 14:52:29', NULL, NULL, 'a:1:{i:0;s:9:\"ROLE_USER\";}', 'Drapeau', 'Grégoire', NULL, 8);
COMMIT;

-- ----------------------------
-- Table structure for account_user_address
-- ----------------------------
DROP TABLE IF EXISTS `account_user_address`;
CREATE TABLE `account_user_address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `street` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `zipcode` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `longitude` double NOT NULL,
  `latitude` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of account_user_address
-- ----------------------------
BEGIN;
INSERT INTO `account_user_address` VALUES (1, '11 rue des Lyces', 'Angers', '49100', 0, 0);
INSERT INTO `account_user_address` VALUES (2, '11 rue des Lyces', 'Angers', '49100', 0, 0);
INSERT INTO `account_user_address` VALUES (3, '11 rue des Lyces', 'Angers', '49100', 0, 0);
INSERT INTO `account_user_address` VALUES (4, '11 rue des Lyces', 'Angers', '49100', 0, 0);
INSERT INTO `account_user_address` VALUES (5, '2 rue du test', 'Angers', '49000', 0, 0);
INSERT INTO `account_user_address` VALUES (6, 'Tst', 'Angers', '49000', 0, 0);
INSERT INTO `account_user_address` VALUES (7, '2 Rue du Test', 'Angers', '49000', 0, 0);
INSERT INTO `account_user_address` VALUES (8, '2 Rue du Test', 'Angers', '49000', 0, 0);
COMMIT;

-- ----------------------------
-- Table structure for allergen
-- ----------------------------
DROP TABLE IF EXISTS `allergen`;
CREATE TABLE `allergen` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `picture` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for category
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of category
-- ----------------------------
BEGIN;
INSERT INTO `category` VALUES (7, 'Au boeuf');
INSERT INTO `category` VALUES (8, 'Au poulet');
INSERT INTO `category` VALUES (9, 'Au poisson');
INSERT INTO `category` VALUES (10, 'Sans bacon');
COMMIT;

-- ----------------------------
-- Table structure for ingredient
-- ----------------------------
DROP TABLE IF EXISTS `ingredient`;
CREATE TABLE `ingredient` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for meal
-- ----------------------------
DROP TABLE IF EXISTS `meal`;
CREATE TABLE `meal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double NOT NULL,
  `picture` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of meal
-- ----------------------------
BEGIN;
INSERT INTO `meal` VALUES (1, 'CHEESE LOVER MENU', 10.39, 'https://ecocea-bk-cdn-prd.azureedge.net/img/menu/b13750c7-014d-47d4-9ee5-9d82bcf94088_@1x');
COMMIT;

-- ----------------------------
-- Table structure for meal_product
-- ----------------------------
DROP TABLE IF EXISTS `meal_product`;
CREATE TABLE `meal_product` (
  `meal_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  PRIMARY KEY (`meal_id`,`product_id`),
  KEY `IDX_AAF9B6B4639666D6` (`meal_id`),
  KEY `IDX_AAF9B6B44584665A` (`product_id`),
  CONSTRAINT `FK_AAF9B6B44584665A` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_AAF9B6B4639666D6` FOREIGN KEY (`meal_id`) REFERENCES `meal` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of meal_product
-- ----------------------------
BEGIN;
INSERT INTO `meal_product` VALUES (1, 1);
INSERT INTO `meal_product` VALUES (1, 3);
COMMIT;

-- ----------------------------
-- Table structure for migration_versions
-- ----------------------------
DROP TABLE IF EXISTS `migration_versions`;
CREATE TABLE `migration_versions` (
  `version` varchar(14) COLLATE utf8mb4_unicode_ci NOT NULL,
  `executed_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of migration_versions
-- ----------------------------
BEGIN;
INSERT INTO `migration_versions` VALUES ('20190403132445', '2019-04-03 13:24:48');
INSERT INTO `migration_versions` VALUES ('20190403135645', '2019-04-03 13:56:48');
INSERT INTO `migration_versions` VALUES ('20190403140130', '2019-04-03 14:01:33');
INSERT INTO `migration_versions` VALUES ('20190403140344', '2019-04-03 14:03:46');
INSERT INTO `migration_versions` VALUES ('20190403140418', '2019-04-03 14:04:20');
COMMIT;

-- ----------------------------
-- Table structure for nutritional
-- ----------------------------
DROP TABLE IF EXISTS `nutritional`;
CREATE TABLE `nutritional` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `informations` longtext COLLATE utf8mb4_unicode_ci COMMENT '(DC2Type:array)',
  `product_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_392E630D4584665A` (`product_id`),
  CONSTRAINT `FK_392E630D4584665A` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for order_product
-- ----------------------------
DROP TABLE IF EXISTS `order_product`;
CREATE TABLE `order_product` (
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  PRIMARY KEY (`order_id`,`product_id`),
  KEY `IDX_2530ADE68D9F6D38` (`order_id`),
  KEY `IDX_2530ADE64584665A` (`product_id`),
  CONSTRAINT `FK_2530ADE64584665A` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_2530ADE68D9F6D38` FOREIGN KEY (`order_id`) REFERENCES `account_order` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of order_product
-- ----------------------------
BEGIN;
INSERT INTO `order_product` VALUES (1, 1);
INSERT INTO `order_product` VALUES (2, 2);
INSERT INTO `order_product` VALUES (3, 2);
INSERT INTO `order_product` VALUES (4, 2);
INSERT INTO `order_product` VALUES (5, 2);
INSERT INTO `order_product` VALUES (6, 2);
INSERT INTO `order_product` VALUES (7, 2);
INSERT INTO `order_product` VALUES (8, 2);
INSERT INTO `order_product` VALUES (9, 2);
INSERT INTO `order_product` VALUES (10, 1);
INSERT INTO `order_product` VALUES (11, 1);
INSERT INTO `order_product` VALUES (12, 1);
INSERT INTO `order_product` VALUES (13, 1);
INSERT INTO `order_product` VALUES (13, 3);
INSERT INTO `order_product` VALUES (14, 1);
INSERT INTO `order_product` VALUES (14, 3);
INSERT INTO `order_product` VALUES (15, 1);
INSERT INTO `order_product` VALUES (16, 1);
INSERT INTO `order_product` VALUES (17, 1);
INSERT INTO `order_product` VALUES (17, 6);
INSERT INTO `order_product` VALUES (18, 2);
INSERT INTO `order_product` VALUES (18, 3);
INSERT INTO `order_product` VALUES (18, 5);
INSERT INTO `order_product` VALUES (18, 6);
INSERT INTO `order_product` VALUES (19, 1);
INSERT INTO `order_product` VALUES (20, 1);
INSERT INTO `order_product` VALUES (21, 1);
INSERT INTO `order_product` VALUES (22, 1);
INSERT INTO `order_product` VALUES (23, 1);
INSERT INTO `order_product` VALUES (24, 2);
INSERT INTO `order_product` VALUES (24, 6);
COMMIT;

-- ----------------------------
-- Table structure for product
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double NOT NULL,
  `picture` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `type_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_D34A04ADC54C8C93` (`type_id`),
  KEY `IDX_D34A04AD12469DE2` (`category_id`),
  CONSTRAINT `FK_D34A04AD12469DE2` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`),
  CONSTRAINT `FK_D34A04ADC54C8C93` FOREIGN KEY (`type_id`) REFERENCES `type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of product
-- ----------------------------
BEGIN;
INSERT INTO `product` VALUES (1, 'CHEESE LOVER ORIGINAL', 6.99, 'https://ecocea-bk-cdn-prd.azureedge.net/img/products/2/49/249b2254-b57c-458e-b190-64b7abb96285_@4x', 'Un pain brioché gratiné au fromage, une sauce fromagère onctueuse, des oignons frits, une tranche de fromage, des tranches de bacon croustillantes et une viande de boeuf grillée à la flamme !\n', 1, 7);
INSERT INTO `product` VALUES (2, 'DOUBLE CHEESE BACON XXL\n', 8.99, 'https://ecocea-bk-cdn-prd.azureedge.net/img/products/8/d4/8d4b953a-912a-4211-a635-16c5ebfa2cc1_@4x', 'Que demander de plus ? Deux viandes de boeuf grillées à la flamme, du fromage et du bacon … Simple, généreux et efficace.', 1, 7);
INSERT INTO `product` VALUES (3, 'FRITES', 2.99, 'https://ecocea-bk-cdn-prd.azureedge.net/img/products/d/91/d917ff26-ea51-4c70-b60a-e5d3a66f4b4b_@2x', 'Comment résister? Dorées, croustillantes et surtout généreuses… nos frites sont parfaites!', 2, NULL);
INSERT INTO `product` VALUES (4, 'Onion Rings', 3.99, 'https://ecocea-bk-cdn-prd.azureedge.net/img/products/2/76/276f7e5e-5312-42ab-8b27-c6651672c5f8_@2x', 'Avec nos onion rings, la boucle est bouclée! Choisissez simplement votre sauce avant de les dévorer.', 2, NULL);
INSERT INTO `product` VALUES (5, 'KING FRIES CHEESE & BACON', 5.99, 'https://ecocea-bk-cdn-prd.azureedge.net/img/products/7/39/739546d1-c5b6-45bc-8465-739f8297f1d1_@4x', 'Des frites, une sauce cheese onctueuse et de croustillants éclats de ... BACON !', 2, NULL);
INSERT INTO `product` VALUES (6, 'CREAMY SHAKES', 3.49, 'https://ecocea-bk-cdn-prd.azureedge.net/img/products/e/a5/ea5a7f3c-328f-4b0a-8bc2-00a7fc17cf0e_@2x', 'Découvrez toute la gourmandise du Shake Crunchy Chocolate ! Un milkshake au chocolat recouvert d\'une onctueuse crème fouettée, de sa sauce chocolat et de délicieux éclats de chocolat croustillants !', 3, NULL);
COMMIT;

-- ----------------------------
-- Table structure for product_allergen
-- ----------------------------
DROP TABLE IF EXISTS `product_allergen`;
CREATE TABLE `product_allergen` (
  `product_id` int(11) NOT NULL,
  `allergen_id` int(11) NOT NULL,
  PRIMARY KEY (`product_id`,`allergen_id`),
  KEY `IDX_EE0F62594584665A` (`product_id`),
  KEY `IDX_EE0F62596E775A4A` (`allergen_id`),
  CONSTRAINT `FK_EE0F62594584665A` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_EE0F62596E775A4A` FOREIGN KEY (`allergen_id`) REFERENCES `allergen` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for product_ingredient
-- ----------------------------
DROP TABLE IF EXISTS `product_ingredient`;
CREATE TABLE `product_ingredient` (
  `product_id` int(11) NOT NULL,
  `ingredient_id` int(11) NOT NULL,
  PRIMARY KEY (`product_id`,`ingredient_id`),
  KEY `IDX_F8C8275B4584665A` (`product_id`),
  KEY `IDX_F8C8275B933FE08C` (`ingredient_id`),
  CONSTRAINT `FK_F8C8275B4584665A` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_F8C8275B933FE08C` FOREIGN KEY (`ingredient_id`) REFERENCES `ingredient` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for restaurant
-- ----------------------------
DROP TABLE IF EXISTS `restaurant`;
CREATE TABLE `restaurant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of restaurant
-- ----------------------------
BEGIN;
INSERT INTO `restaurant` VALUES (1, 'BURGER KING ANGERS', 'Angers');
INSERT INTO `restaurant` VALUES (2, 'BURGER KING NANTES', 'Nantes');
INSERT INTO `restaurant` VALUES (3, 'BURGER KING PARIS', 'Paris 4ème');
COMMIT;

-- ----------------------------
-- Table structure for type
-- ----------------------------
DROP TABLE IF EXISTS `type`;
CREATE TABLE `type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `picture` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of type
-- ----------------------------
BEGIN;
INSERT INTO `type` VALUES (1, 'Burgers', NULL);
INSERT INTO `type` VALUES (2, 'Snacks', NULL);
INSERT INTO `type` VALUES (3, 'Délices', NULL);
INSERT INTO `type` VALUES (4, 'Boissons chaudes', NULL);
INSERT INTO `type` VALUES (5, 'P\'tits plaisirs', NULL);
INSERT INTO `type` VALUES (6, 'Salades', NULL);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
