<?php

namespace App\Repository\Product;

use App\Entity\Product\Nutritional;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Nutritional|null find($id, $lockMode = null, $lockVersion = null)
 * @method Nutritional|null findOneBy(array $criteria, array $orderBy = null)
 * @method Nutritional[]    findAll()
 * @method Nutritional[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NutritionalRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Nutritional::class);
    }

    // /**
    //  * @return Nutritional[] Returns an array of Nutritional objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('n.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Nutritional
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
