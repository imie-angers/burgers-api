<?php

namespace App\EventListener;

use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;
use App\Entity\Account\User;

class AuthenticationSuccessListener
{
	/**
	 * @param AuthenticationSuccessEvent $event
	 */
	public function onAuthenticationSuccessResponse(AuthenticationSuccessEvent $event)
	{
	    $data = $event->getData();
	    $user = $event->getUser();

	    if (!$user instanceof User) {
	        return;
	    }	

	    $data['data'] = [
	        'id' => $user->getId(),
	        'lastname' => $user->getLastname(),
	        'firstname' => $user->getFirstname(),
	        'email' => $user->getEmail(),
	    ];

	    $event->setData($data);
	}
}
