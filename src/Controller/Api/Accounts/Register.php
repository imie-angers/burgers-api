<?php
namespace App\Controller\Api\Accounts;

use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use FOS\UserBundle\Model\UserManagerInterface;
use App\Entity\Account\User;
use Symfony\Component\HttpFoundation\Response;

class Register
{
    protected $em;
    protected $container;
    protected $userManager;
    protected $request;

    public function __construct(
        RequestStack $requestStack,
        EntityManagerInterface $em, 
        ContainerInterface $container,
        UserManagerInterface $userManager,
        JWTTokenManagerInterface $JWTManager
    )
    {
        $this->request = $requestStack;
        $this->em = $em;
        $this->container = $container;
        $this->userManager = $userManager;
        $this->JWTManager = $JWTManager;
    }

    public function __invoke($data)
    {
        $request = $this->request->getCurrentRequest()->request;
        $user = $data;

        $user->setPassword($user->getPlainPassword());
        $user->setEnabled(true);

        $validator = $this->container->get('validator');
        $errors = $validator->validate($user);
        $ouput = [];

        foreach($errors as $error)
        {
            $ouput[$error->getPropertyPath()] = [
               $error->getPropertyPath() => $error->getMessage()
            ];
        }


        if($errors->count() == 0)
        {
            $this->userManager->updateUser($user);
            $user->setToken($this->JWTManager->create($user));
        }
        
        return $user;
    }
}