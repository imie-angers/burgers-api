<?php

namespace App\Entity\Account;

use ApiPlatform\Core\Annotation\ApiResource;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Controller\Api\Accounts\Register;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * User
 *
 * @ORM\Table(name="account_user")
 * @ApiResource(
 *     collectionOperations={
 *          "register"={
 *              "method"="POST",
 *              "path"="/users/register",
 *              "controller"=Register::class,
 *              "normalization_context"={"groups"={
 *                  "register"
 *              }},
 *              "denormalization_context"={"groups"={
 *                  "user-write"
 *              }},
 *          },
 *     },
 *     itemOperations={
 *          "get"={
 *              "access_control"="is_granted('ROLE_USER') and object == user", 
 *              "method"="GET"
 *          },
 *          "put"={
 *              "access_control"="is_granted('ROLE_USER') and object == user", 
 *              "method"="PUT"
 *          }
 *     }
 * )
 * @ORM\Entity()
 * @UniqueEntity("email")
 */
class User extends BaseUser
{
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"user", "register"})
     */
    protected $id;
    
    /**
     * @Groups({"user", "edit-profile", "register", "user-write"})
     * @Assert\Email()
     * @Assert\NotBlank()
     */
    protected $email;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"user", "register", "edit-profile", "user-write"})
     * @Assert\NotBlank()
     */
    protected $firstname;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"user", "register", "edit-profile", "user-write"})
     * @Assert\NotBlank()
     */
    protected $lastname;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $phone;

    /**
     * @Groups({"user-write"})
     */
    protected $plainPassword;

    protected $username;

    public function isUser(UserInterface $user = null)
    {
        return $user instanceof self && $user->id === $this->id;
    }
    
    public function __construct(){
        parent::__construct();
        // your own logic
        $this->roles = array('ROLE_USER');
        $this->gender = 0;
    }

    /**
     * Sets the email.
     *
     * @param string $email
     * @return User
     */
    public function setEmail($email){
        $this->setUsername($email);
        return parent::setEmail($email);
    }

    /**
     * Set the canonical email.
     *
     * @param string $emailCanonical
     * @return User
     */
    public function setEmailCanonical($emailCanonical){
        $this->setUsernameCanonical($emailCanonical);
        return parent::setEmailCanonical($emailCanonical);
    }

    /**
     * @return mixed
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * @param mixed $firstname
     *
     * @return self
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @param mixed $lastname
     *
     * @return self
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     *
     * @return self
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    /**
     * @param mixed $plainPassword
     *
     * @return self
     */
    public function setPlainPassword($plainPassword)
    {
        $this->plainPassword = $plainPassword;

        return $this;
    }

    /**
     * @Groups({"register"})
     */
    private $token;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Account\UserAddress", cascade={"persist", "remove"})
     * @Groups({"register", "user-write"})
     */
    private $address;

    /**
     * @return mixed
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param mixed $token
     *
     * @return self
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    public function getAddress(): ?UserAddress
    {
        return $this->address;
    }

    public function setAddress(?UserAddress $address): self
    {
        $this->address = $address;

        return $this;
    }
}
