<?php

namespace App\Entity\Product;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\RangeFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Annotation\ApiFilter;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     collectionOperations={"get"={"method"="GET", "normalization_context"={"groups"={"products"}}}},
 *     itemOperations={"get"={"method"="GET"}},
 *     attributes={"order"={"name": "ASC"}}
 * )
 * @ApiFilter(RangeFilter::class, properties={"price"})
 * @ApiFilter(
 *  SearchFilter::class, 
 *  properties={
 *      "name": "partial",
 *      "type": "exact",
 *      "category": "exact"
 *  }
 * )
 * @ORM\Entity(repositoryClass="App\Repository\Product\ProductRepository")
 */
class Product
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"products", "meals", "order"})
     */
    private $name;

    /**
     * @ORM\Column(type="float")
     * @Groups({"products", "meals", "order"})
     */
    private $price;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"products", "meals", "order"})
     */
    private $picture;

    /**
     * @ORM\Column(type="text")
     * @Groups({"products", "order"})
     */
    private $body;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Product\Ingredient")
     */
    private $ingredients;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Product\Type", inversedBy="products")
     * @Groups({"products"})
     */
    private $type;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Product\Allergen")
     */
    private $allergens;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Product\Nutritional", mappedBy="product")
     */
    private $nutritionals;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Product\Category", inversedBy="products")
     */
    private $category;

    public function __construct()
    {
        $this->ingredients = new ArrayCollection();
        $this->allergens = new ArrayCollection();
        $this->nutritionals = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getPicture(): ?string
    {
        return $this->picture;
    }

    public function setPicture(string $picture): self
    {
        $this->picture = $picture;

        return $this;
    }

    public function getBody(): ?string
    {
        return $this->body;
    }

    public function setBody(string $body): self
    {
        $this->body = $body;

        return $this;
    }

    /**
     * @return Collection|Ingredient[]
     */
    public function getIngredients(): Collection
    {
        return $this->ingredients;
    }

    public function addIngredient(Ingredient $ingredient): self
    {
        if (!$this->ingredients->contains($ingredient)) {
            $this->ingredients[] = $ingredient;
        }

        return $this;
    }

    public function removeIngredient(Ingredient $ingredient): self
    {
        if ($this->ingredients->contains($ingredient)) {
            $this->ingredients->removeElement($ingredient);
        }

        return $this;
    }

    public function getType(): ?Type
    {
        return $this->type;
    }

    public function setType(?Type $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return Collection|Allergen[]
     */
    public function getAllergens(): Collection
    {
        return $this->allergens;
    }

    public function addAllergen(Allergen $allergen): self
    {
        if (!$this->allergens->contains($allergen)) {
            $this->allergens[] = $allergen;
        }

        return $this;
    }

    public function removeAllergen(Allergen $allergen): self
    {
        if ($this->allergens->contains($allergen)) {
            $this->allergens->removeElement($allergen);
        }

        return $this;
    }

    /**
     * @return Collection|Nutritional[]
     */
    public function getNutritionals(): Collection
    {
        return $this->nutritionals;
    }

    public function addNutritional(Nutritional $nutritional): self
    {
        if (!$this->nutritionals->contains($nutritional)) {
            $this->nutritionals[] = $nutritional;
            $nutritional->setProduct($this);
        }

        return $this;
    }

    public function removeNutritional(Nutritional $nutritional): self
    {
        if ($this->nutritionals->contains($nutritional)) {
            $this->nutritionals->removeElement($nutritional);
            // set the owning side to null (unless already changed)
            if ($nutritional->getProduct() === $this) {
                $nutritional->setProduct(null);
            }
        }

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }
}
