<?php

namespace App\Entity\Product;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\RangeFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Annotation\ApiFilter;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     collectionOperations={"get"={"method"="GET", "normalization_context"={"groups"={"meals"}}}},
 *     itemOperations={"get"={"method"="GET"}},
 *     attributes={"order"={"name": "ASC"}}
 * )
 * @ApiFilter(RangeFilter::class, properties={"price"})
 * @ApiFilter(
 *  SearchFilter::class, 
 *  properties={
 *      "name": "partial"
 *  }
 * )
 * @ORM\Entity(repositoryClass="App\Repository\Product\MealRepository")
 */
class Meal
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"meals"})
     */
    private $name;

    /**
     * @ORM\Column(type="float")
     * @Groups({"meals"})
     */
    private $price;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"meals"})
     */
    private $picture;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Product\Product")
     * @Groups({"meals"})
     */
    private $products;

    public function __construct()
    {
        $this->products = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPrice(): ?float
    {
        $total = 0;

        foreach ($this->getProducts() as $product) {
            $total += $product->getPrice();    
        }

        return $total;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getPicture(): ?string
    {
        return $this->picture;
    }

    public function setPicture(string $picture): self
    {
        $this->picture = $picture;

        return $this;
    }

    /**
     * @return Collection|Product[]
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function addProduct(Product $product): self
    {
        if (!$this->products->contains($product)) {
            $this->products[] = $product;
        }

        return $this;
    }

    public function removeProduct(Product $product): self
    {
        if ($this->products->contains($product)) {
            $this->products->removeElement($product);
        }

        return $this;
    }
}
