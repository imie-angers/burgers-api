security:
    encoders:
        App\Entity\Account\User:
            algorithm: bcrypt

    role_hierarchy:
        ROLE_ADMIN:       ROLE_USER
        ROLE_SUPER_ADMIN: ROLE_ADMIN

    providers:
        entity_provider:
            entity:
                class: App\Entity\Account\User
                property: username

    firewalls:
        login:
            pattern:  ^/api/login
            stateless: true
            anonymous: true
            json_login:
                check_path: /api/login_check
                success_handler: lexik_jwt_authentication.handler.authentication_success
                failure_handler: lexik_jwt_authentication.handler.authentication_failure

        register:
            pattern:  ^/api/users/register$
            stateless: true
            anonymous: true

        facebook:
            pattern:  ^/api/facebook/connect$
            stateless: true
            anonymous: true

        password:
            pattern:  ^/api/users/password$
            stateless: true
            anonymous: true

        api:
            pattern:  ^/api
            stateless: true
            anonymous: true
            #anonymous: false
            provider: entity_provider
            guard:
                authenticators:
                    - lexik_jwt_authentication.jwt_token_authenticator
        main:
            pattern: ^/
            form_login:
                login_path: fos_user_security_login
                check_path: fos_user_security_check
                provider: entity_provider
                csrf_token_generator: security.csrf.token_manager

            logout:       
                path: fos_user_security_logout
            anonymous:    true

    access_control:
        - { path: ^/api/login_check$, role: IS_AUTHENTICATED_ANONYMOUSLY }
        - { path: ^/api/users/register$, role: IS_AUTHENTICATED_ANONYMOUSLY }
        - { path: ^/api, roles: IS_AUTHENTICATED_ANONYMOUSLY }
        # IS_AUTHENTICATED_FULLY